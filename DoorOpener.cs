﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Forms;

namespace DoorFucker
{
    class DoorOpener
    {
        private string _webpage;
        private string _openName;
        private string _closeName;

        private HtmlElement _openButton;
        private HtmlElement _closeButton;

        private bool _opened;
        private bool _pageLoaded;
        private WebBrowser _browser;

        private System.Timers.Timer _timer;


        public int TempTest { get; set; }
        public string Webpage { get { return _webpage; } set { _webpage = value; } }
        public string OpenName { get { return _openName; } set { _openName = value; } }
        public string CloseName { get { return _closeName; } set { _openName = value; } }
        public double Interval { get { return _timer.Interval; } }

        public DoorOpener(string webpage, string openName, string closeName)
        {
            System.Threading.Thread.CurrentThread.SetApartmentState(ApartmentState.STA);
            this._webpage = webpage;
            this._openName = openName;
            this._closeName = closeName;
            _opened = false;

            _timer = new System.Timers.Timer();
            _timer.Interval = 40000;
            _timer.Elapsed += _timer_Elapsed;
            _browser = new WebBrowser();
        }

        void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (_opened)
            {
                _closeButton.InvokeMember("Click");
                Console.WriteLine("Closing door!");
            }
            else
            {
                _openButton.InvokeMember("Click");
                Console.WriteLine("Opening door!");
            }

            _opened = !_opened;
        }

        private void runBrowserThread(string url)
        {
            var th = new Thread(() =>
            {
                _browser.DocumentCompleted += br_DocumentCompleted;
                _browser.Navigate(url);
                Application.Run();
            });
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
        }

        void br_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            _openButton = _browser.Document.GetElementById(_openName);
            _closeButton = _browser.Document.GetElementById(_closeName);

            if (_openButton == null || _closeButton == null)
            {
                Console.WriteLine("Did not load the webpage correctly, aborting launch. \nAre you sure you set the right values?");
            }
            else
            {
                Console.WriteLine("Succesfully loaded the webpage and found the buttons, started the timer with an interval of " + _timer.Interval + " milliseconds");
                _timer.Start();
            }

        }

        public void Start()
        {
            runBrowserThread(_webpage);
            Console.WriteLine("Loading the webpage: " + _webpage);
        }

        public void Stop()
        {
            Console.WriteLine("Stopping the DoorOpener");

            _timer.Stop();
            _openButton = null;
            _closeButton = null;
        }
        
    }
}
