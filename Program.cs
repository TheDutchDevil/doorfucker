﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DoorFucker
{
    class Program
    {
        private static readonly string WEBPAGE = "http://v3a.nl/door.php";

        private DoorOpener _opener;

        [STAThread]
        static void Main(string[] args)
        {
            Program prgrm = new Program();


            Console.Write("Welcome to the doorfucker, use this to make Stijn's door go crazy! \n\n'Start' starts the program,  'Stop' stops it.");
            Console.WriteLine("\n'List' prints out a list of all editable variables. \n'Set' 'VariableName' 'NewValue' sets the value of public variables");
            
            Console.Write("[CMD]: ");
            while (true)
            {
                prgrm.ReadLine(Console.ReadLine().Split(' '));
                Console.Write("[CMD]: ");
            }
        }

        public Program()
        {
            _opener = new DoorOpener(WEBPAGE, "3", "1");
        }

        private void ReadLine(string[] args)
        {
            string command = args[0].ToLower();
            if (command == "start")
            {
                _opener.Start();
            }
            else if (command == "stop")
            {
                _opener.Stop();
            }
            else if (command == "list")
            {
                Console.WriteLine("Printing out all public properties: ");

                PrintProperties(_opener);
            }
            else if (command == "set")
            {
                if (!CheckParams(args, 3))
                {
                    return;
                }
                SetProperty(_opener, args[1], args[2]);

            }
            else
            {
                Console.WriteLine("ERROR: Unknown command!");
            }
        }

        private void SetProperty(object toSet, string propName, string newValue)
        {
            PropertyInfo property = toSet.GetType().GetProperty(propName);

            if (property == null)
            {
                Console.WriteLine("No property found by this name!");
                return;
            }
            if (!property.CanWrite)
            {
                Console.WriteLine("This property is read only!");
                return;
            }
            if (property.PropertyType.Name != "String")
            {
                Console.WriteLine("Only properties of the type string can be set through this method!");
                return;
            }

                property.SetValue(toSet, newValue);

                Console.WriteLine("Set was successful!");
                PrintProperty(property);
        }

        private void PrintProperties(object toPrint)
        {
            PropertyInfo[] propInfo = toPrint.GetType().GetProperties();

            foreach (PropertyInfo pi in propInfo)
            {
                if (pi.CanWrite)
                {
                    PrintProperty(pi);
                }
            }

            Console.WriteLine("Printing out all readonly properties: ");
            foreach (PropertyInfo pi in propInfo)
            {
                if (!pi.CanWrite)
                {
                    PrintProperty(pi);
                }
            }
        }

        private void PrintProperty(PropertyInfo pi)
        {
            Console.WriteLine("\tType: " + pi.PropertyType.Name + " Name: '" + pi.Name + "' Value: '" + pi.GetValue(_opener) + "'");
        }

            public bool CheckParams(string[] args, int required)
            {
                if (args.Length < required)
                {
                    Console.WriteLine("Invalid number of arguments provided!");
                    return false;
                }
                else
                {
                    return true;
                }
            }
        
    }
}
